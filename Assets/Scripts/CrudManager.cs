using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrudManager : MonoBehaviour
{

    public GameObject listHolder, playerDetail, inputFields, updateItem;

    // Start is called before the first frame update
    void Start()
    {
        ReadItem();
    }

    public void ReadItem() 
    {
        int count = PlayerPrefs.GetInt("count");

        for (int i = 0; i < listHolder.transform.childCount; i++) {
            Destroy(listHolder.transform.GetChild(i).gameObject);
        }

        int num = 0;
        for (int i = 0; i <= count; i++) {
            num++;
            string id = PlayerPrefs.GetString("id[" + i + "]");

            string name = PlayerPrefs.GetString("name[" + i + "]");
            string health = PlayerPrefs.GetString("health[" + i +"]");
            string level = PlayerPrefs.GetString("level[" + i +"]");

            if (id != "")
            {
                GameObject temp_item = Instantiate(playerDetail, listHolder.transform);
                temp_item.name = i.ToString();
                temp_item.transform.GetChild(0).GetComponent<Text>().text = num.ToString();
                temp_item.transform.GetChild(1).GetComponent<Text>().text = name;
                temp_item.transform.GetChild(2).GetComponent<Text>().text = health;
                temp_item.transform.GetChild(3).GetComponent<Text>().text = level;
            }
            else {
                num--;
            }
        }

    }

    public void Create() {
        int count = PlayerPrefs.GetInt("count");
        count++;
        PlayerPrefs.SetString("id[" + count + "]", count.ToString());
        PlayerPrefs.SetString("name[" + count + ']', inputFields.transform.GetChild(1).GetComponent<InputField>().text);
        PlayerPrefs.SetString("health[" + count + ']', inputFields.transform.GetChild(2).GetComponent<InputField>().text);
        PlayerPrefs.SetString("level[" + count + ']', inputFields.transform.GetChild(3).GetComponent<InputField>().text);
        PlayerPrefs.SetInt("count", count);
        inputFields.transform.GetChild(1).GetComponent<InputField>().text = "";
        inputFields.transform.GetChild(2).GetComponent<InputField>().text = "";
        inputFields.transform.GetChild(3).GetComponent<InputField>().text = "";
        ReadItem();
    }

    public void Delete(GameObject playerDetail) {
        string id_pref = playerDetail.name;
        PlayerPrefs.DeleteKey("id[" + id_pref + "]");
        PlayerPrefs.DeleteKey("name[" + id_pref +"]");
        PlayerPrefs.DeleteKey("health[" + id_pref + "]");
        PlayerPrefs.DeleteKey("level[" + id_pref + "]");
        ReadItem();
    }

    string id_edit;

    public void ListUpdate(GameObject obj_edit)
    {
        updateItem.SetActive(true);
        id_edit = PlayerPrefs.GetString("id[" + obj_edit.name + "]");
        updateItem.transform.GetChild(1).GetComponent<InputField>().text = PlayerPrefs.GetString("name["+ obj_edit.name +"]");
        updateItem.transform.GetChild(2).GetComponent<InputField>().text = PlayerPrefs.GetString("health["+obj_edit.name+"]");
        updateItem.transform.GetChild(3).GetComponent<InputField>().text = PlayerPrefs.GetString("level["+obj_edit.name+"]");
    }
    
    // Update is called once per frame
    public void EditedList()
    {
        PlayerPrefs.SetString("name[" + id_edit + "]", updateItem.transform.GetChild(1).GetComponent<InputField>().text);
        PlayerPrefs.SetString("health[" + id_edit + "]", updateItem.transform.GetChild(2).GetComponent<InputField>().text);
        PlayerPrefs.SetString("level[" + id_edit + "]", updateItem.transform.GetChild(3).GetComponent<InputField>().text);
        ReadItem();

    }

    private void Update()
    {
        
    }
}
